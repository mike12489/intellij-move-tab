import org.jetbrains.intellij.tasks.PatchPluginXmlTask
import org.jetbrains.intellij.tasks.PublishTask

buildscript {
    repositories { mavenCentral() }
    dependencies { classpath(kotlin("gradle-plugin", version = "1.3.61")) }
}

repositories {
    mavenCentral()
}

plugins {
    base
    kotlin("jvm") version "1.3.61"
    id("org.jetbrains.intellij") version "0.4.14"
    id("com.gradle.build-scan") version "3.1"
}

group = "com.mikejhill"
version = "1.1.0"

intellij {
    pluginName = "MoveTab"
    type = "IU"
    version = "2019.3"
}

buildScan {
    termsOfServiceUrl = "https://gradle.com/terms-of-service"
    termsOfServiceAgree = "yes"
}

val patchPluginXml by tasks.existing(PatchPluginXmlTask::class) {
    pluginId("com.mikejhill.intellij.movetab")
    sinceBuild("193.5233")
    changeNotes(file("$projectDir/docs/CHANGELOG.html").readText())
}

val publishPlugin by tasks.existing(PublishTask::class) {
    val publishToken: String? by project.extra
    val publishUsername: String? by project.extra
    val publishPassword: String? by project.extra
    setToken(publishToken)
    setUsername(publishUsername)
    setPassword(publishPassword)
}

tasks.withType<Wrapper> {
    distributionType = Wrapper.DistributionType.ALL
}
